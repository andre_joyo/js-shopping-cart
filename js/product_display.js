var id_product;
function display_product() {
    index=localStorage.getItem("index_product");
    content=document.getElementById("content");
    div_product=document.createElement("div");
    div_product.className="div-product";
    div_img=document.createElement("div");
    img=document.createElement("img");
    img.setAttribute("src", "images/"+products[index]["image-route"]);
    content.appendChild(div_product);
    div_product.appendChild(div_img);
    div_img.appendChild(img);
    name_product=document.createElement("p");
    t_name = document.createTextNode(products[index]["brand"]+" "+products[index]["model"]);
    div_product.appendChild(name_product);
    name_product.appendChild(t_name);
    t_price=document.createTextNode(products[index]["price"][0][0][0]);
    price_product=document.createElement("p");
    div_product.appendChild(price_product);
    price_product.appendChild(t_price);
    t_basic_description=document.createTextNode(products[index]["description"]);
    basic_description_product=document.createElement("p");
    div_product.appendChild(basic_description_product);
    basic_description_product.appendChild(t_basic_description);    

    div_description_product=document.createElement("div");
    keys_0 = Object.keys(products[index]["price"]);//console.log(keys_0);//n_keys_0 = keys_0.length;console.log(n_keys_0);
        keys_0.forEach(function(item0) {//console.log(keys_0[item0]);
            keys_1=Object.keys(products[index]["price"][keys_0[item0]]);//console.log(keys_1);
            keys_1.forEach(function(item1) {
                keys_2=Object.keys(products[index]["price"][keys_0[item0]][keys_1[item1]]);//console.log(keys_2);
                keys_2.forEach(function(item2) {//console.log(item2);//console.log(item0+" "+item1+" "+item2);
                    description="";
                    description=description+JSON.stringify(products[index]["size_screen"][item0])+description+JSON.stringify(products[index]["ram"][item1])+description+JSON.stringify(products[index]["storage"][item2]);
                    //console.log(products[index]["price"][item0][item1][item2]);console.log(description);
                    t_button_description=document.createTextNode(description);
                    button_description_product=document.createElement("button");
                    button_description_product.className="buttons_description";
                    button_description_product.id=item0+"-"+item1+"-"+item2;
                    div_product.appendChild(div_description_product);
                    div_description_product.appendChild(button_description_product);
                    button_description_product.appendChild(t_button_description);
                })
            })
        });
    buttons_description=document.getElementsByClassName("buttons_description");
    buttons_description[0].className+=" selected_option";
    for (let i = 0; i < buttons_description.length; i++) {
        buttons_description[i].addEventListener("click", function(){
            for (let j = 0; j < buttons_description.length; j++) {//console.log(buttons_description.length);
                buttons_description[j].classList.remove("selected_option");                
            }
            event.target.className+=" selected_option";
            id_product=event.target.id;//console.log(event.target.id);//console.log("option button clicked");
        })        
    }
    buttons_description[0].click();

    div_buttons_add_extract=document.createElement("div");
    button_add=document.createElement("button");
    button_add.className="add";
    t_add=document.createTextNode("Add 1 to cart");
    button_extract=document.createElement("button");
    button_extract.className="right extract";
    t_extract=document.createTextNode("Extract 1 from cart");
    button_add.appendChild(t_add);
    button_extract.appendChild(t_extract);
    div_product.appendChild(button_add);
    div_product.appendChild(button_extract);

    buttons_add=document.getElementsByClassName("add");
    buttons_extract=document.getElementsByClassName("extract");
    for (let i = 0; i < buttons_add.length; i++) {
        buttons_add[i].addEventListener("click", function () {
            index=event.target.parentNode.firstChild.id.slice(3);//alert(index);
            action="add";
            comprove_cart(true);
        });
        
        buttons_extract[i].addEventListener("click", function () {
            index=event.target.parentNode.firstChild.id.slice(3);//alert(index);
            action="extract";
            comprove_cart(true);
        });
    }
}
function comprove_cart() {
    cart=false;
    if (action=="add" || action=="extract") {
        for (let i = 0; i < Object.keys(localStorage).length; i++) {//console.log(i+" "+localStorage.key(i));        
            if (localStorage.key(i)=="cart"){
                cart=true;
            }        
        }    
        if (cart==true){
            update_json_cart();
        } else{
            create_json_cart();
        }
    }
}
function create_json_cart() {
    if (action=="add"){
        json_cart='[{"'+localStorage.getItem("index_product")+'":{"'+id_product+'":"1"}}]';
    }
    action="";
    localStorage.setItem("cart", json_cart);//console.log(json_cart);
}
var value_key;
function update_json_cart() {
    var key_found=false;
    json_cart=localStorage.getItem("cart");
    json_cart=JSON.parse(json_cart);//  json_cart=JSON.stringify(json_cart);console.log(json_cart);
    stock=products[localStorage.getItem("index_product")]["stock"];//console.log(stock);
    var ID_product_found=false;
    var key_ID;
    if (action=="add") {
        for (let key in json_cart) {//console.log(key+"-"+json_cart[key]);console.log("key: "+key+" - index: "+index);console.log("action: "+action);
                for (let key2 in json_cart[key]) {
                    if (key2==localStorage.getItem("index_product")) {//ID PRODUCTO ENCONTRADO?
                        ID_product_found=true;
                        key_ID=key;
                        for (let key3 in json_cart[key][key2]) {
                            var sto0=key3[0];
                            var sto1=key3[2];
                            var sto2=key3[4];
                            if (key3==id_product) { //if sub_id found
                                key_found=true;
                                if (json_cart[key][key2][key3]<products[key]["stock"][sto0][sto1][sto2]) {
                                    json_cart[key][key2][key3]=parseInt(json_cart[key][key2][key3])+1;
                                    json_cart[key][key2][key3]=""+json_cart[key][key2][key3]+"";                                            
                                }
                            }
                        }
                    }
                }   
        }
        if (ID_product_found==true && key_found==false) {//console.log(json_cart);
            json_cart[key_ID][localStorage.getItem("index_product")][id_product]="1";//console.log(index);console.log(JSON.stringify(json_cart));
        }
        if (ID_product_found==false) {
            var objs={};
            objs[id_product]="1";
            var objs2={};
            objs2[localStorage.getItem("index_product")]=objs;
            json_cart.push(objs2);
        }    
    }else if (action=="extract") {          //else(el if no hace falta)
        var product_index_deleted=false;
        for (let key in json_cart) {    
            for (let key2 in json_cart[key]) {
                if (key2==localStorage.getItem("index_product")) {                
                    for (let key3 in json_cart[key][key2]) {//console.log(json_cart[key][key2][key3]);
                        if (key3==id_product && parseInt(json_cart[key][key2][key3])>0) {
                            json_cart[key][key2][key3]-=1;
                            if (json_cart[key][key2][key3]==0) {
                                product_index_deleted=true;
                                delete json_cart[key][key2][key3];
                            }
                        }
                        if (product_index_deleted==true) {//console.log(JSON.stringify(json_cart[key][key2]));
                            if (JSON.stringify(json_cart[key][key2])=="{}") {
                                delete json_cart[key];
                                json_cart.splice(key, 1);
                            }
                        }
                    }
                }
            }
        }
    }
    action="";
    json_cart=JSON.stringify(json_cart);
    localStorage.setItem("cart", json_cart);
}
