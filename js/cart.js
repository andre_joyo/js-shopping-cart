var index=localStorage.getItem("index_product");
function comprove_cart() {
    cart=false;
    if (action=="add" || action=="extract") {
        for (let i = 0; i < Object.keys(localStorage).length; i++) {        
            if (localStorage.key(i)=="cart"){
                cart=true;
            }        
        }    
        if (cart==true){
            update_json_cart();
        } else{
            create_json_cart();
        }
    }
    if (window.location=="file:///C:/Users/afjog/Desktop/Assembler/assembler_d31/js-shopping-cart/cart.html") {
        table_cart.innerHTML="";
        tr=document.createElement("tr");
        for (let i = 0; i < 8; i++) {
            td=document.createElement("td");  
            td.className="eight";   
            tr.appendChild(td);     
        }
        table_cart.appendChild(tr);
        trh=document.createElement("tr");
        for (let i = 0; i < 5; i++) {
            td=document.createElement("td");    
            if (i==0) {
                t=document.createTextNode("Image");                    
                td.setAttribute("colspan", "1");
            } else if (i==1) {
                t=document.createTextNode("Item");                    
                td.setAttribute("colspan", "4");
            } else if(i==2){
                t=document.createTextNode("Price");                    
                td.setAttribute("colspan", "1");
            } else if (i==3){
                t=document.createTextNode("Qty");                    
                td.setAttribute("colspan", "1")
            } else{
                t=document.createTextNode("Total");                    
                td.setAttribute("colspan", "1");
            }
            td.appendChild(t);
            trh.appendChild(td);                
        }
        table_cart.appendChild(trh);
        display_cart();
    }
}
function create_json_cart() {
    if (action=="add"){
        json_cart='{"'+index+'":"1"}';
    }
    action="";
    localStorage.setItem("cart", json_cart);
}
var value_key;
function update_json_cart() {
    var key_found=false;
    var id_product="0-0-0";
    json_cart=localStorage.getItem("cart");
    json_cart=JSON.parse(json_cart);
    var curr_index=parseInt(event.target.parentNode.firstChild.id.slice(3));
    if (isNaN(curr_index)) {
        curr_index=event.target.parentNode.parentNode.className;
        id_product=event.target.parentNode.className;
    }
    stock=products[curr_index]["stock"];
    var ID_product_found=false;
    var key_ID;
                                
    if (action=="add") {
        for (let key in json_cart) {
                for (let key2 in json_cart[key]) {
                    if (key2==curr_index) {//ID PRODUCTO ENCONTRADO?
                        ID_product_found=true;
                        key_ID=key;
                        for (let key3 in json_cart[key][key2]) {
                            var sto0=key3[0];
                            var sto1=key3[2];
                            var sto2=key3[4];
                            if (key3==id_product) { //if sub_id found
                                key_found=true;
                                if (json_cart[key][key2][key3]<products[key2]["stock"][sto0][sto1][sto2]) {
                                    json_cart[key][key2][key3]=parseInt(json_cart[key][key2][key3])+1;
                                    json_cart[key][key2][key3]=""+json_cart[key][key2][key3]+"";                                            
                                }
                            }
                        }
                    }
                }   
        }
        if (ID_product_found==true && key_found==false) {
            json_cart[key_ID][curr_index][id_product]="1";
        }
        if (ID_product_found==false) {
            var objs={};
            objs[id_product]="1";
            var objs2={};
            objs2[curr_index]=objs;
            json_cart.push(objs2);
        }    
    }else if (action=="extract") {          //else(el if no hace falta)
        for (let key in json_cart) {    
            for (let key2 in json_cart[key]) {
                if (key2==curr_index) {    
                    for (let key3 in json_cart[key][key2]) {
                        if (key3==id_product && parseInt(json_cart[key][key2][key3])>0) {
                            json_cart[key][key2][key3]-=1;
                            if (json_cart[key][key2][key3]==0) {
                                delete json_cart[key][key2][key3];
                            }
                        }
                    }
                }
            }
        }
    }
    action="";
    json_cart=JSON.stringify(json_cart);
    localStorage.setItem("cart", json_cart);
}
function body_loaded() {
    display_cart();
    document.body.click();
}
function display_cart() {
    json_cart=localStorage.getItem("cart");
    table_cart=document.getElementById("table_cart");
    if (json_cart!=null) {
        json_cart=JSON.parse(json_cart);
        for (let key in json_cart) { 
            for (let key2 in json_cart[key]) {
                for (let key3 in json_cart[key][key2]) {       
                    tr=document.createElement("tr");
                    tr.className=key2;
                    var pr0=key3[0];
                    var pr1=key3[2];
                    var pr2=key3[4];
                    for (let i = 0; i < 5; i++) {
                        td=document.createElement("td");    
                        if (i==0) {
                            t=document.createElement("img");
                            t.setAttribute("src", "images/"+products[key2]["image-route"]);
                            td.setAttribute("colspan", "1")
                            td.className=key3;
                        } else if (i==1) {
                            var brand=products[key2]["brand"];
                            var model=products[key2]["model"];
                            var s_c=products[key2]["size_screen"][pr0];
                            var ram=products[key2]["ram"][pr1];
                            var storage=products[key2]["storage"][pr2];
                            t=document.createTextNode(brand+" "+model+" "+s_c+" "+ram+" "+storage);                    
                            td.setAttribute("colspan", "4")
                            td.className=key3;
                        } else if(i==2){
                            console.log(pr0+" "+pr1+" "+pr2);
                            t=document.createTextNode(products[key2]["price"][pr0][pr1][pr2]);
                            td.setAttribute("colspan", "1");
                            td.className="price "+key3;
                        } else if (i==3){        
                            b_a=document.createElement("button");
                            t_b_a=document.createTextNode("+");
                            b_a.appendChild(t_b_a);
                            b_a.id="a"+key;
                            b_a.addEventListener("click", function () {
                                action="add";
                                index=key;
                                comprove_cart();
                                table_cart.innerHTML="";
                                tr=document.createElement("tr");
                                for (let i = 0; i < 8; i++) {
                                    td=document.createElement("td");  
                                    td.className="eight";   
                                    tr.appendChild(td);     
                                }
                                table_cart.appendChild(tr);

                                trh=document.createElement("tr");
                                for (let i = 0; i < 5; i++) {
                                    td=document.createElement("td");    
                                    if (i==0) {
                                        t=document.createTextNode("Image");                    
                                        td.setAttribute("colspan", "1");
                                    } else if (i==1) {
                                        t=document.createTextNode("Item");                    
                                        td.setAttribute("colspan", "4");
                                    } else if(i==2){
                                        t=document.createTextNode("Price");                    
                                        td.setAttribute("colspan", "1");
                                    } else if (i==3){
                                        t=document.createTextNode("Qty");                    

                                        td.setAttribute("colspan", "1")
                                    } else{
                                        t=document.createTextNode("Total");                    
                                        td.setAttribute("colspan", "1");
                                    }
                                    td.appendChild(t);
                                    trh.appendChild(td);                
                                }
                                table_cart.appendChild(trh);
                                
                                display_cart(); 
                            });
                            t=document.createTextNode(json_cart[key][key2][key3]);
                            td.setAttribute("colspan", "1");
                            td.className=key3;
                            b_e=document.createElement("button");
                            t_b_e=document.createTextNode("-");
                            b_e.appendChild(t_b_e);
                            b_e.id="e"+key;
                            b_e.addEventListener("click", function () {
                                action="extract";
                                index=key;
                                comprove_cart();
                                table_cart.innerHTML="";
                                tr=document.createElement("tr");
                                for (let i = 0; i < 8; i++) {
                                    td=document.createElement("td");  
                                    td.className="eight";   
                                    tr.appendChild(td);     
                                }
                                table_cart.appendChild(tr);

                                trh=document.createElement("tr");
                                for (let i = 0; i < 5; i++) {
                                    td=document.createElement("td");    
                                    if (i==0) {
                                        t=document.createTextNode("Image");                    
                                        td.setAttribute("colspan", "1");
                                    } else if (i==1) {
                                        t=document.createTextNode("Item");                    
                                        td.setAttribute("colspan", "4");
                                    } else if(i==2){
                                        t=document.createTextNode("Price");                    
                                        td.setAttribute("colspan", "1");
                                    } else if (i==3){
                                        t=document.createTextNode("Qty");                    

                                        td.setAttribute("colspan", "1")
                                    } else{
                                        t=document.createTextNode("Total");                    
                                        td.setAttribute("colspan", "1");
                                    }
                                    td.appendChild(t);
                                    trh.appendChild(td);                
                                }
                                table_cart.appendChild(trh);
                                
                                display_cart(); 
                            });
                        } else{
                            price=parseInt(products[key2]["price"][pr0][pr1][pr2]);
                            t=document.createTextNode(price*json_cart[key][key2][key3]+" eur");
                            td.setAttribute("colspan", "1");
                            td.className="total";
                        }
                        if (i==3) {
                            tr.appendChild(td);
                            td.appendChild(b_a);   
                            td.appendChild(t);   
                            td.appendChild(b_e);   
                        } else{
                            td.appendChild(t);
                            tr.appendChild(td);                
                        }
                    }    
                    table_cart.appendChild(tr);
                }
            }
        }     
    } else{
        table_cart.innerHTML="";
        h3=document.createElement("h3");
        t=document.createTextNode("No hay artículos en el carrito");
        table_cart.appendChild(h3);
        h3.appendChild(t);
    }
    document.body.addEventListener('click', function(){     
        sum=0;
        total=document.getElementsByClassName("total");
        for (let i = 0; i < total.length; i++) {
            sum=sum+parseInt(total[i].innerHTML);           
        } 
        total_span=document.getElementById("total_span");
        if (document.getElementsByClassName("price").length>0) {
            price0=document.getElementsByClassName("price")[0].innerHTML;
            currency=price0.slice(length-4);
            total_span.innerHTML=sum+currency;   
        }
    });
    imgs=document.getElementsByTagName("img");//console.log(imgs);
    for (let i = 0; i < imgs.length; i++) {
        imgs[i].addEventListener("click", function(){
            localStorage.setItem("index_product", this.parentNode.parentNode.id);
            window.location="product.html";
        })        
    }
}
function to_dollars() {
    prices=document.getElementsByClassName("price");
    total=document.getElementsByClassName("total");
    for (let i = 0; i < prices.length; i++) {//console.log(Math.ceil(parseInt(prices[i].innerHTML)*1.11));
        dollar_price=Math.ceil(parseInt(prices[i].innerHTML)*1.11);
        prices[i].innerHTML=dollar_price+" usd";
        dollar_total=Math.ceil(parseInt(total[i].innerHTML)*1.11);
        total[i].innerHTML=dollar_total+" usd";        
    }
    to_dollar=document.getElementById("to_dollar");
    to_dollar.style.display="none";
    to_euro=document.getElementById("to_euro");
    to_euro.style.display="block";
}
function to_euros() {
    table_cart.innerHTML="";
    tr=document.createElement("tr");
    for (let i = 0; i < 8; i++) {
        td=document.createElement("td");  
        td.className="eight";   
        tr.appendChild(td);     
    }
    table_cart.appendChild(tr);

    trh=document.createElement("tr");
    for (let i = 0; i < 5; i++) {
        td=document.createElement("td");    
        if (i==0) {
            t=document.createTextNode("Image");                    
            td.setAttribute("colspan", "1");
        } else if (i==1) {
            t=document.createTextNode("Item");                    
            td.setAttribute("colspan", "4");
        } else if(i==2){
            t=document.createTextNode("Price");                    
            td.setAttribute("colspan", "1");
        } else if (i==3){
            t=document.createTextNode("Qty");                    

            td.setAttribute("colspan", "1")
        } else{
            t=document.createTextNode("Total");                    
            td.setAttribute("colspan", "1");
        }
        td.appendChild(t);
        trh.appendChild(td);                
    }
    table_cart.appendChild(trh);
    display_cart();
    to_euro=document.getElementById("to_euro");
    to_euro.style.display="none";
    to_dollar=document.getElementById("to_dollar");
    to_dollar.style.display="block";
}
function pay() {
    window.location="pay.html"
}
