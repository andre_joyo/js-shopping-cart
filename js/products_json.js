var products=[
    {
        "brand": "acer",
        "model": "swift-3",
        "image-route": "acer-swift-3.jpg",
        "description": "Procesador Intel Core i5 8250U, Sistema operativo Windows 10 Home, Tipo de pantalla Full HD",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"799 eur",
                    "1":"839 eur"
                },
                "1":{
                    "0":"869 eur",
                    "1":"899 eur",
                }
            },
            "1":{
                "0":{
                    "0":"929 eur",
                    "1":"959 eur"
                },
                "1":{
                    "0":"989 eur",
                    "1":"1019 eur",
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                }
            }
        }   
    },
    {
        "brand": "acer",
        "model": "aspire-3",
        "image-route": "acer-aspire-3.jpg",
        "description": "Procesador Intel Core i3 7020U, Sistema operativo Windows 10 Home, Tipo de pantalla Acer ComfyView LCD",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB",
            "2":"1024 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"399 eur",
                    "1":"429 eur"
                },
                "1":{
                    "0":"459 eur",
                    "1":"489 eur",
                    "2":"525 eur"
                },
            },
            "1":{
                "0":{
                    "0":"529 eur",
                    "1":"559 eur"
                },
                "1":{
                    "0":"589 eur",
                    "1":"619 eur",
                    "2":"645 eur"
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                    "2":"7"
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                    "2":"8"
                }
            }
        }   
    },
    {
        "brand": "asus",
        "model": "f-40",
        "image-route": "asus-f-40.jpg",
        "description": "Procesador Intel Core i3 8145U, Sistema operativo Windows 10 Home, Tipo de pantalla Full HD",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"499 eur",
                    "1":"529 eur"
                },
                "1":{
                    "0":"559 eur",
                    "1":"589 eur",
                }
            },
            "1":{
                "0":{
                    "0":"629 eur",
                    "1":"659 eur"
                },
                "1":{
                    "0":"789 eur",
                    "1":"819 eur",
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                }
            }
        }   
    },
    {
        "brand": "asus",
        "model": "f-50",
        "image-route": "asus-f-50.jpg",
        "description": "Procesador Intel Core i3 7020U, Sistema operativo Windows 10 Home, Tipo de pantalla HD",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"499 eur",
                    "1":"529 eur"
                },
                "1":{
                    "0":"559 eur",
                    "1":"589 eur",
                }
            },
            "1":{
                "0":{
                    "0":"629 eur",
                    "1":"659 eur"
                },
                "1":{
                    "0":"689 eur",
                    "1":"719 eur",
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                }
            }
        }   
    },
    {
        "brand": "lenovo",
        "model": "yoga",
        "image-route": "lenovo-yoga.jpg",
        "description": "Procesador Intel Core i3 7020U, Sistema operativo Windows 10 Home, Tipo de pantalla TN",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"599 eur",
                    "1":"639 eur"
                },
                "1":{
                    "0":"659 eur",
                    "1":"699 eur",
                }
            },
            "1":{
                "0":{
                    "0":"729 eur",
                    "1":"759 eur"
                },
                "1":{
                    "0":"789 eur",
                    "1":"839 eur",
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                }
            }
        }   
    },
    {
        "brand": "lg",
        "model": "gram",
        "image-route": "lg-gram.jpg",
        "description": "Procesador Intel Core i5 8265U, Sistema operativo Windows 10 Home, Tipo de pantalla FHD IPS",
        "size_screen":{
            "0":"14 pulgadas",
            "1":"15.6 pulgadas"
        },
        "ram":{
            "0":"4 GB",
            "1":"8 GB"
        },
        "storage":{
            "0":"256 GB",
            "1":"512 GB"
        },
        "price": {
            "0":{
                "0":{
                    "0":"599 eur",
                    "1":"629 eur"
                },
                "1":{
                    "0":"659 eur",
                    "1":"689 eur",
                }
            },
            "1":{
                "0":{
                    "0":"719 eur",
                    "1":"749 eur"
                },
                "1":{
                    "0":"779 eur",
                    "1":"809 eur",
                }
            }
        },
        "stock": {
            "0":{
                "0":{
                    "0":"7",
                    "1":"8"
                },
                "1":{
                    "0":"6",
                    "1":"8",
                }
            },
            "1":{
                "0":{
                    "0":"9",
                    "1":"5"
                },
                "1":{
                    "0":"8",
                    "1":"9",
                }
            }
        }   
    }
] 
